# Sign UP Service

## 1 - Add Database Stubbing

```javascript
app.post('/setup', setupDatabaseMock)
const stubbedUser = sinon.stub(User, 'find');

function setupDatabaseMock(request, result) {
    const state = request.body.state

    stubbedUser.reset();
    stubbedUser
        .callsFake(() => {
            return Promise.reject("Unexpected call to find User");
        });

    switch (state) {
        case 'a state':
            stubbedUser
                .withArgs({ field: "value" })
                .callsFake(() => {
                    return Promise.resolve(users);
                });
            break;
        default:
            throw new Error('Unexpected State')
    }

    result.end()
}
```

## 2 - Start Mock Server

```javascript
const port = 9999;
const server = app.listen(port, () => {
    console.log(`Sign Up Service listening on http://localhost:${port}`)
})

after(() => {
    return server.close();
})
```

## 3 - Add Test

```javascript
xit('should validate the expectations of Location Promotion Service', function () {
        this.timeout(10000)

        const pactBroker = process.env.PACT_BROKER || "http://localhost:8082"

        let opts = {
            providerBaseUrl: `http://localhost:${port}`,
            providerStatesSetupUrl: `http://localhost:${port}/setup`,
            pactUrls: [`${pactBroker}/pacts/provider/Sign%20up%20Service/consumer/Location%20Promotion%20Service/latest`],
            publishVerificationResult: true,
            providerVersion: '1.0.0'
        }

        return verifier.verifyProvider(opts)
            .then(output => {
                console.log('Pact Verification Complete!')
                console.log(output)
            })
            .finally(() => server.close())
    })
```

### Stub Active Record

```javascript
const stubbedUser = sinon.stub(User, 'find');

...

stubbedUser
    .callsFake( () => {
        return Promise.reject("Unexpected call to find User");
    });

...

switch(state) {
    case 'a state' :
        ...
        stubbedUser
            .withArgs({field: "value"})
            .callsFake( () => {
                return Promise.resolve(users);
            });
        break;
    default:
        throw new Error('Unexpected State')
}
```

## Implementation

### 1 - Add Route

```javascript
userRouter.get("/users", (request, response) => {
    const location = request.query.location;
});
```

### 2 - Query by Location

```javascript
User
.find({
    location: location
})
.then( (users, err) => {
    
});
```

### 3 - Return result

```javascript
if (err) {
    response.status(500).end();
    return;
}
response.status(200).json(users);
```
