const Router = require('express').Router;

module.exports = function (User) {
    var router = Router();
    
    router.post('/users', (request, response) => {
        const user = new User(request.body);
        user.save().then(
            (savedUser) => {
                console.log(`saved user: ${savedUser}`);
                response.render('signup-success',
                    { user: savedUser }
                );
            },
            (err) => {
                console.log("error " + err);
                response.statusCode = 500;
                response.end();
            }
        )
    });

    return router;
}