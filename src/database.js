const mongoose = require('mongoose');

const Database = {}

Database.initialize = () => {
    const mongoHost = process.env.MONGO_HOST || "localhost"
    mongoose.connect(`mongodb://${mongoHost}/users`, { useNewUrlParser: true });
    var db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function () {
        console.log("database up and running!");
    });
}

module.exports = Database