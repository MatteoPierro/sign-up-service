const express = require('express');

module.exports = function() {
    const app = express();
    app.use(express.json());
    app.use(express.urlencoded({ extended: true }));
    app.use(express.static('public'));
    app.set('view engine', 'jade');
    app.set('views', __dirname + '/../views');
    return app;
}