const Application = require('../../src/application')
const ApiRouter = require('../../src/router/api/apiRouter')
const Bar = require("../../src/model/bar");
const verifier = require('pact').Verifier
const chai = require('chai')
const chaiAsPromised = require('chai-as-promised')
chai.use(chaiAsPromised).should();

const app = new Application()
app.use(new ApiRouter(Bar))

app.post('/setup', setupDatabaseMock)
function setupDatabaseMock(request, result) {
    const state = request.body.state

    switch (state) {
        case 'Has bar':
            Bar.findOne = () => {
                return Promise.resolve({ bar: 'bar' });
            }
            break
        case 'Has no bar':
            Bar.findOne = () => {
                return Promise.resolve(null);
            }
            break
        default:
            throw new Error('Unexpected State')
    }

    result.end()
}

describe('Pact Verification', () => {
    const port = 9999;
    const server = app.listen(port, () => {
        console.log(`Sign Up Service listening on http://localhost:${port}`)
    })

    after(() => {
        return server.close();
    })

    it('should validate the expectations of Bar proxy Service', function () {
        this.timeout(10000)

        const pactBroker = process.env.PACT_BROKER || "http://localhost:8082"

        let opts = {
            providerBaseUrl: `http://localhost:${port}`,
            providerStatesSetupUrl: `http://localhost:${port}/setup`,
            pactUrls: [`${pactBroker}/pacts/provider/Sign%20up%20Service/consumer/Bar%20proxy%20Service/latest`],
            publishVerificationResult: true,
            providerVersion: '1.0.0'
        }

        return verifier.verifyProvider(opts)
            .then(output => {
                console.log('Pact Verification Complete!')
                console.log(output)
            })
    })
})
