const Router = require('express').Router;
const UserRouter = require('./userRouter');

module.exports = function (User) {
    var guiRouter = Router();

    guiRouter.use(new UserRouter(User));

    return guiRouter;
}