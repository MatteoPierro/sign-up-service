const mongoose = require('mongoose');

const User = mongoose.model('User', { 
    first_name: String,
    last_name: String,
    birthday: Date,
    location: {
        type: String,
        enum: ["Belgium", "Holland", "France", "UK", "Italy"],
        default: "Belgium"
    },
    email: String,
    password: String
});

module.exports = User