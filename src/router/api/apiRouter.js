const Router = require('express').Router;
const BarRouter = require('./barRouter');
const UserRouter = require('./userRouter');

module.exports = function (Bar, User) {
    var routes = Router();
    routes.use(new BarRouter(Bar));
    routes.use(new UserRouter(User));

    var apiRouter = Router();
    apiRouter.use('/api', routes);

    return apiRouter;
}