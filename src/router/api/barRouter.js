const Router = require('express').Router;
const BarController = require('../../controller/api/barController')

module.exports = function (Bar) {
    const controller = BarController(Bar);

    var barRouter = Router();

    barRouter.post('/bar', controller.newBar);
    barRouter.get('/bar', controller.oneBar);
    barRouter.get('/bar/count', controller.countBars);

    return barRouter;
}