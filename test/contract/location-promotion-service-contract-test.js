const Application = require('../../src/application')
const Bar = require("../../src/model/bar");
const User = require("../../src/model/user");
const ApiRouter = require('../../src/router/api/apiRouter')
const verifier = require('pact').Verifier
const chai = require('chai')
const sinon = require('sinon')
const chaiAsPromised = require('chai-as-promised')
chai.use(chaiAsPromised).should();

const app = new Application()
app.use(new ApiRouter(Bar, User))

describe('Pact Verification', () => {

})