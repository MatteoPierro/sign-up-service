module.exports = function (Bar) {
    const barController = {}
    
    barController.newBar = (request, response) => {
        const bar = new Bar({ bar: 'bar' });
        bar.save().then(
            (bar) => {
                response.status(201).json(bar);
            },
            (err) => {
                response.status(500).end();
            });
    }
    
    barController.oneBar = (request, response) => {
        Bar.findOne().then(
            (bar) => {
                if (!bar) {
                    response.statusCode = 404;
                    response.end();
                    return;
                }
    
                response.json(bar);
            },
            (err) => {
                console.log("error " + err);
                response.statusCode = 500;
                response.end();
            }
        );
    }
    
    barController.countBars = (request, response) => {
        Bar.countDocuments().then(
            (count) => {
                response.status(200).send(count.toString());
            },
            (err) => {
                console.log("error " + err);
                response.statusCode = 500;
                response.end();
            }
        );
    }

    return barController;
}