const Application = require('./src/application')
const Database = require('./src/database')
const Bar = require("./src/model/bar");
const User = require("./src/model/user");
const ApiRouter = require('./src/router/api/apiRouter');
const GuiRouter = require('./src/router/gui/guiRouter');

Database.initialize();

const application = new Application();

application.use(new GuiRouter(User))
application.use(new ApiRouter(Bar, User));

application.listen('7777', () => {
    console.log('web-server up and running!');
});